@extends('layout.master')

@section('judul')
Halaman Film
@endsection

@push('styles')
<link href="https://cdn.datatables.net/v/bs4/dt-2.0.7/datatables.min.css" rel="stylesheet">
@endpush

@push('scripts')
<script src="https://cdn.datatables.net/v/bs4/dt-2.0.7/datatables.min.js"></script>
<script>
    let table = new DataTable('#myTable');
</script>
@endpush

@section('content')
<a href="/film/create" class="btn btn-primary btn-sm">Tambah</a><br><br>
<div class="row">
    @forelse ($film as $item )
    <div class="col-4">
        <div class="card">
            <img src="{{ asset('images/'.$item->poster) }}" class="card-img-top" width="100px" alt="">
            <div class="card-body">
                <h5 class="card-title">{{ $item->judul}}</h5>
                <p class="card-text">{{ $item->ringkasan }}</p>
                <a href="/film/{{ $item->id }}" class="btn btn-primary btn-block">Detail</a>
                <div class="row my-3">
                    <div class="col">
                        <a href="/film/{{ $item->id }}/edit" class="btn btn-warning btn-block">Edit</a>
                    </div>
                    <div class="col">
                        <form action="/film/{{ $item->id }}" method="post">
                            @csrf
                            @method('delete')

                            <input type="submit" class="btn btn-danger btn-block" value="Delete">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
    @empty
        
    @endforelse
</div>
@endsection