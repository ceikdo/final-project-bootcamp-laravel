@extends('layout.master')

@section('judul')
Halaman Detail Film
@endsection

@section('content')

<img src="{{asset('images/'.$film->poster) }}" width="100%" alt="">

<h1 class="text-primary">{{ $film->judul }}</h1>
<p>{{ $film->ringkasan }}</p>

@endsection