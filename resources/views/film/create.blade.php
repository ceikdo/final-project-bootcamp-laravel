@extends('layout.master')

@section('judul')
Halaman Tambah Film
@endsection

@section('content')
<form action="/film" method="post" enctype="multipart/form-data">
    <!-- Validation -->
    @if ($errors->any())
     <div class="alert alert-danger">
         <ul>
             @foreach ($errors->all() as $error)
             <li>{{ $error }}</li>
             @endforeach
         </ul>
     </div>
    @endif

    @csrf
    <div class="form-group">
        <label>Genre</label><br>
        <select name="genre_id" id="" class="form-control">
            <option value="">-- Pilih Genre --</option>
            @forelse ($genre as $item )
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @empty
            <option value="">Tidak Ada Genre</option>
            @endforelse
        </select>
     </div>

     <div class="form-group">
        <label>Judul Film</label><br>
        <input type="text" name="judul" class="form-control">
     </div>

     <div class="form-group">
        <label>Ringkasan</label><br>
        <textarea name="ringkasan" class="form-control" cols="30" rows="10"></textarea>
     </div>

     <div class="form-group">
        <label>Tahun</label><br>
        <input type="number" name="tahun" class="form-control">
     </div>

     <div class="form-group">
        <label>Poster</label>
        <input type="file" name="poster" class="form-control">
     </div>
    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
</form>
@endsection
