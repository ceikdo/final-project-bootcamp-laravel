@extends('layout.master')

@section('judul')
Tambah Genre 
@endsection

@section('content')
<form action="/genre" method="post">
    <!--Validation-->
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @csrf
    <div class="form-group">
        <label>Nama Genre</label><br>
        <input type="text" name="nama" class="form-control">
    </div>
    <button type="submit" class="btn btn-primary btn-sm"> Submit</button>
</form>
@endsection
