@extends('layout.master')

@section('judul')
Detail {{ $genre->nama }}
@endsection

@push('styles')
<link href="https://cdn.datatables.net/v/bs4/dt-2.0.7/datatables.min.css" rel="stylesheet">
@endpush

@push('scripts')
<script src="https://cdn.datatables.net/v/bs4/dt-2.0.7/datatables.min.js"></script>
<script>
    let table = new DataTable('#myTable');
</script>
@endpush

@section('content')
<p>Nama : {{ $genre->nama }}</p>

<a href="/genre" class="btn btn-primary btn-sm">Kembali</a>
@endsection