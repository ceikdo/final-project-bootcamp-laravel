@extends('layout.master')

@section('judul')
Halaman Edit Genre
@endsection

@section('content')
<form action="/genre/{{$genre->id}}" method="post">
    @csrf
    @method('PUT')
    <label>Nama Lengkap</label><br>
    <input type="text" name="nama" value="{{ $genre->nama }}" class="form-control"><br>
    @error('nama')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
    @enderror
    <input type="submit" class="btn btn-primary btn-sm">
</form>
@endsection
