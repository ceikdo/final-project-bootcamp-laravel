@extends('layout.master')

@section('judul')
Halaman Cast Film
@endsection

@push('styles')
<link href="https://cdn.datatables.net/v/bs4/dt-2.0.7/datatables.min.css" rel="stylesheet">
@endpush

@push('scripts')
<script src="https://cdn.datatables.net/v/bs4/dt-2.0.7/datatables.min.js"></script>
<script>
    let table = new DataTable('#myTable');
</script>
@endpush

@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm">Tambah</a><br><br>
<div class="card">
  <!-- /.card-header -->
  <div class="card-body">
    <table id="myTable" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>Nama Lengkap</th>
        <th>Umur</th>
        <th>Bio</th>
        <th>Aksi</th>
      </tr>
      </thead>
      <tbody>
        @forelse($cast as $value)
        <tr>
            <td>{{ $value->nama }}</td>
            <td>{{ $value->umur }}</td>
            <td>{{ $value->bio }}</td>
            <td>
              <form action="/cast/{{ $value->id }}" method="post">
                @csrf
                @method('DELETE')
                <a href="/cast/{{ $value->id }}" class="btn btn-info btn-sm">Detail</a>
                <a href="/cast/{{ $value->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
              </form>
            </td>
          </tr>
          @empty
          <td colspan="3">Tidak ada data</td>
        @endforelse
      </tbody>
    </table>
  </div>
  <!-- /.card-body -->
</div>
@endsection