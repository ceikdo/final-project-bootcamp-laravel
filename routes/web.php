<?php

use App\Http\Controllers\FilmController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);

// CRUD HALAMAN FILM
// Create -> Form tambah data
Route::get('/film/create', [FilmController::class, 'create']);
// Untuk menambah data ke database
Route::post('/film', [FilmController::class, 'store']);
//Read->Menampilkan semua data
Route::get('/film', [FilmController::class, 'index']);
// Menampilkan detail film berdasarkan index
Route::get('/film/{film_id}', [FilmController::class, 'show']);

// Update -> Mengubah data
// Form update data
Route::get('/film/{film_id}/edit', [FilmController::class, 'edit']);
// Update data ke database berdasarkan id
Route::put('/film/{film_id}', [FilmController::class, 'update']);

// Delete -> Menghapus data
// Delete data di database berdasarkan id
Route::delete('/film/{film_id}', [FilmController::class, 'destroy']);



// CRUD HALAMAN GENRE
// Create -> Form tambah data
Route::get('/genre/create', [GenreController::class, 'create']);
// Untuk menambah data ke database
Route::post('/genre', [GenreController::class, 'store']);

//Read->Menampilkan semua data
Route::get('/genre', [GenreController::class, 'index']);
// Menampilkan detail genre berdasarkan index
Route::get('/genre/{genre_id}', [GenreController::class, 'show']);

// Update -> Mengubah data
// Form update data
Route::get('/genre/{genre_id}/edit', [GenreController::class, 'edit']);
// Update data ke database berdasarkan id
Route::put('/genre/{genre_id}', [GenreController::class, 'update']);

// Delete -> Menghapus data
// Delete data di database berdasarkan id
Route::delete('/genre/{genre_id}', [GenreController::class, 'destroy']);



// CRUD HALAMAN CAST
// Create -> Form tambah data
Route::get('/cast/create', [CastController::class, 'create']);
// Untuk menambah data ke database
Route::post('/cast', [CastController::class, 'store']);

// Read -> Menampilkan semua data
Route::get('/cast', [CastController::class, 'index']);
// Menampilkan detail cast berdasarkan index
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

// Update -> Mengubah data
// Form update data
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
// Update data ke database berdasarkan id
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

// Delete -> Menghapus data
// Delete data di database berdasarkan id
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
