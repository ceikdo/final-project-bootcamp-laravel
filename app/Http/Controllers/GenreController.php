<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GenreController extends Controller
{
    public function create(){
        return view('genre.create');
    }

    public function store(Request $request){

    // dd($request->all());
    $request->validate([
        'nama' => 'required'
    ]);

    DB::table('genres')->insert([
        'nama' => $request->input('nama')
    ]);

    return redirect('/genre');
    }

    public function index(){
        $genre = DB::table('genres')->get();

        return view('genre.index', ['genre' => $genre]);
    }

    public function show($id){
        $genre = DB::table('genres')->where('id', $id)->first();
        return view('genre.show', ['genre' => $genre]);
    }

    public function edit($id){
        $genre = DB::table('genres')->where('id', $id)->first();

        return view('genre.edit', ['genre' => $genre]);
    }

    public function update(Request $request, $id){
        $request->validate([
            'nama' => 'required',
        ]);

        DB::table('genres')->where('id', $id)->update([
            'nama' => $request->nama,
        ]);

        return redirect('/genre');
    }

    public function destroy($id){
        DB::table('genres')->where('id', $id)->delete();

        return redirect('/genre');
    }
}
