<?php

namespace App\Http\Controllers;

use App\Models\Film;
use Illuminate\Http\Request;
use App\Models\Genre;
use File;


class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $film = Film::get();

        return view('film.index', ['film' => $film]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $genre = Genre::get();

        return view('film.create', ['genre' =>$genre ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required|mimes:jpg,jpeg,png|max:2048',
            'genre_id' => 'required'
        ]);

        $posterName = time().'.'.$request->file('poster')->extension();

        $request->file('poster')->move(public_path('images'), $posterName);

        $film = new Film();

        $film->judul = $request->input('judul');
        $film->ringkasan = $request->input('ringkasan');
        $film->tahun = $request->input('tahun');
        $film->genre_id = $request->input('genre_id');
        $film->poster = $posterName;
        
        $film->save();

        return redirect('/film');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $film = Film::find($id);

        return view('film.detail', ['film' => $film]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $film = Film::find($id);
        $genre = Genre::get();

        return view('film.edit', ['film' => $film, 'genre' => $genre]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'genre_id' => 'required'
        ]);

        $film = Film::find($id);

        if($request->has('poster')){
            // Hapus gambar lama
            $path = 'images/';
            File::delete($path . $film->poster);

            // Gambar baru
            $posterName = time(). '.' .$request->file('poster')->extension();

            $request->file('poster')->move(public_path('images'), $posterName);

            // Ganti data di database
            $film->poster = $posterName;
        }

        $film->judul = $request->input('judul');
        $film->ringkasan = $request->input('ringkasan');
        $film->tahun = $request->input('tahun');
        $film->genre_id = $request->input('genre_id');
        
        $film->save();

        return redirect('/film');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $film = Film::find($id);

        $path = 'images/';
        File::delete($path . $film->poster);

        $film->delete();

        return redirect('/film');
    }
}
